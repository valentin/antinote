import os
import sys
from datetime import datetime

from flask import Flask

from .views import views
from .database import Database

def create_app():
    sys.path.insert(0, os.path.dirname(__file__))

    app = Flask(__name__)
    app.config.from_object("config")

    app.jinja_env.globals.update({
        "year": datetime.now().year,
        "menuitems": [
            ('/', '<i class="fa-solid fa-house-chimney"></i>', 'accueil'),
            ('/grades/', 'Mes notes', ''),
            ('/timetable/', 'Emploi du temps', '')
        ]
    })

    db = Database(app)
    views(app, db)

    return app

app = create_app()