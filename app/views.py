import os
from hashlib import sha256
from functools import wraps

from flask import render_template, send_from_directory, request, session, redirect

from .database import Session


def views(app, db):
    def est_connecte():
        return session.get("uuid") is not None and db.check_connection(session.get("uuid"))

    def login_required(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs) if est_connecte() else redirect("/login")
        return wrapper

    @app.route("/")
    @login_required
    def index():
        return render_template("index.html", s=Session(db, session["uuid"]))

    @app.route("/login/", methods=["GET"])
    def login_get():
        return render_template("login.html")

    @app.route("/login/", methods=["POST"])
    def login_post():
        session["uuid"] = db.create_session(
            request.form["username"],
            sha256(request.form["password"].encode()).hexdigest()
        )

        return redirect("/") if session["uuid"] else redirect("/login?error=1")

    @app.route("/logout/")
    @login_required
    def logout():
        db.destroy_session(session["uuid"])
        session["uuid"] = None
        return redirect("/login")

    @app.route("/grades/")
    @login_required
    def grades():
        s = Session(db, session["uuid"])
        g = s.fetch_grades()
        return render_template("grades.html", s=s, grades=g)

    @app.route("/timetable/")
    @login_required
    def timetable():
        return render_template("timetable.html", s=Session(db, session["uuid"]))

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
                                   'favicon.ico', mimetype='image/vnd.microsoft.icon')
